### 1. Create an cloud volume

```bash
gcloud compute disks create --size=10GB --zone=us-east4-a nfs-disk
```

### 2. Setup the nfs server

Apply the nfs-server pod

```bash
kubectl create ns provider-nfs
kubectl create -f nfs-server-deployment.yaml
```

### 3. Create the volume

```bash
kubectl create -f nfs-volume.yaml
```

### 4. Create a pod that consumes the volume

```bash
kubectl create -f  nfs-test-pod.yaml
```

### 5. Setup the gatekeeper stuff

```bash
kubectl apply -f https://raw.githubusercontent.com/open-policy-agent/gatekeeper/v3.5.1/deploy/experimental/gatekeeper-mutation.yaml
```

That command will get us some mutation CRDs. Then we apply our mutation!

```bash
kubectl apply -f mutation.yaml
```

### 6. Test it out with a pod

```bash
kubectl apple -f nfsless-test-pod.yaml
```

Once the pod starts, `exec -it` in and see if `/nfs` has what is expected.

### 7. Add this namespace to Coder as a workspace provoder

Your newly created workspaces in this workspace provider should all
have the `/nfs` share. All should be read and write access.

Currently the CVM mechanism does nkt support passing a volume
mount to the Docker-enabled contsiners. 
