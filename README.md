# Example of Mutating Coder Workspaces

The current statue of coder workspaces is that only a few configurations are
exposed to users through the configurations. Exploring the ability for system
administrators to add customizations via Mutating Admission Controllers such
as Gatekeeper.

Subdirectories in this repo will contain the resources needed to configure
various types of admission hooks.

## Gatekeeper by OpenPolicyAgent

For any that use Gatekeeper, the [Gatekeeper installation](https://open-policy-agent.github.io/gatekeeper/website/docs/install)
is run.

```bash
kubectl apply -f https://raw.githubusercontent.com/open-policy-agent/gatekeeper/release-3.5/deploy/gatekeeper.yaml
```

## Custom 

Found [this article](https://medium.com/ovni/writing-a-very-basic-kubernetes-mutating-admission-webhook-398dbbcb63ec)
which may be helpful.

It looks like a much more involved process to make a custom admission controller
though it is super powerful and doesn't rely on Gatekeeper or OPA to implement
capabilities.

Example repo: https://github.com/morvencao/kube-mutating-webhook-tutorial
